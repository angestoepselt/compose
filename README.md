# compose your linux

This repo contains all files to modify the system after it has been installed.

Please make sure to create the file `.secrets.env` with the following content:

```
SNIPEIT_APIKEY=     # https://snipe-it.readme.io/reference/generating-api-tokens
SNIPEIT_DOMAIN=     # orga.z31.it
REPO=               # The URL of the Repo were your want to get the angestoepselt-info info Script, like https://codeberg.org/angestoepselt/compose
BRANCH=             # the Branach of the REPO, default at codeberg.org is main
```

```
    Options:
    INFO:
        -h|--help                   print this info
        -v|--version                show version of script

        -a|--all                    running all task below

        -u|--update                 perform an basic system update
        -i|--install                install all packages for this system
        -r|--register               Register PC in snipe-IT Asset Management
        -c|--configure              running basic system config settings
        -d|--files_download         download useful files to your HomeDirectory
        -m|--modify_dm              paint your Desktop Manager with angestoepselt design
        --checkinfo                 Check if PC is already in snipe-IT present
        --clean                     clean up trash, like files and from system tasks like packagemanger
```

---


### Take care of snipeIT Custom fields at system_register

```
{
  "status_id": "2",
  "name": "$Model",
  "model_id": "$model",
  "serial": "$serialno",
  "_snipeit_mac_address_1": "$HW_ADDRESS",
  "_snipeit_betriebssystem_2": "$Distro",
  "_snipeit_festplatte_4": "$HDD",
  "_snipeit_prozessor_5": "$CPU",
  "_snipeit_arbeitsspeicher_6": "$Memory",
  "_snipeit_optisches_laufwerk_7": "$Optical_Drive",
  "_snipeit_display_8": "$display",
  "_snipeit_akku_9": "$Battery_life",
  "_snipeit_anydeskid_10": "$Anydesk_ID"
}
```

### fetch all informations about Custom Fields with an API request
```
export $(cat .secrets-env) && \
curl --silent --request GET --url "https://$SNIPEIT_DOMAIN/api/v1/fields" --header 'accept: application/json' --header 'authorization: Bearer '$SNIPEIT_APIKEY'' | jq .rows[].db_column_name
```